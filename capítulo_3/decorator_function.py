def decorator_funcion(decor_func):  # El decorador-función requiere una función para el argumento
    def wrapper(*args, **kwargs):  # Aquí envolvemos la función original y pasamos sus argumentos al decorador
        print()
        print("Args son:", args)
        print("Kwargs son:", kwargs)
        decor_func(*args, **kwargs)
    return wrapper


@decorator_funcion
def test1(string, **kwargs):
    print(string, "Mundo!")

    for clave, valor in kwargs.items():  # clave = Hello, valor = World!
        print(clave, valor)


test1("Hola", Hello="World!")
