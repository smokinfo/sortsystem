def outer_function():
    mensaje = "Pertenezco a la outer_function"

    def nested_function():
        print(mensaje)
    return nested_function
    # Hacemos el objeto closure devolviendo el objeto de la función nested_function que recuerda la variable mensaje


enclosure_object = outer_function()
# Asignamos a enclosure_object el objeto que ha devuelto outer_function()
# En este punto el código ya no sabe que string contiene la variable mensaje de la outer_function()
# porque lo acaba de terminar de ejecutar y no hemos almacenado previamente esta variable por ningún lado.
# Pero el objeto closure sigue teniendo esta variable dentro de su código.

print("Enclosure object:", enclosure_object)

enclosure_object()  # Ejecutamos el objeto closure en otro contexto. Closure imprime el mensaje de su función exterior

