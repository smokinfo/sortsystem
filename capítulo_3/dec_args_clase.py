class DecoratorClaseConArgs:
    def __init__(self, arg1, arg2, arg3):  # La función para decorar no se pasa aquí, aquí pasan los argumentos
        print("Dentro de DecoratorClaseConArgs.__init__()")
        self.arg1 = arg1
        self.arg2 = arg2
        self.arg3 = arg3

    def __call__(self, funcion):  # La función para decorar pasa aquí
        print("Dentro de DecoratorClaseConArgs.__call__()")

        def wrap(*args):  # Envolvemos la función original
            print("Dentro de wrap()")
            print("Argumentos de decorador:", self.arg1, self.arg2, self.arg3)
            funcion(*args)
            print("Después de funcion(*args)")
        return wrap


@DecoratorClaseConArgs("Aquí", "pasamos", "otros datos")
def test(arg1, arg2, arg3):
    print("Argumentos de test:", arg1, arg2, arg3)


print("Después de decorar")

print("\nLlamamos a función varias veces\n")
test("Hola", "Mundo!", "¿Qué tal?")
print("Despues de primera llamada\n")
test("Hello", "World!", "What's up?")
print("Despues de segunda llamada\n")
