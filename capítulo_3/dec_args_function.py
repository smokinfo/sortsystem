def decorator_function_with_args(arg1, arg2, arg3):  # Aquí pasamos los argumentos de decorador

    def wrap(function):  # Envolvemos la función original al llamar a esta función decorada
        print("Dentro de wrap()")

        def wrapped_function(*args):  # Obtenemos los argumentos de la función decorada
            print("Dentro de wrapped_function()")
            print("Argumentos de decorador:", arg1, arg2, arg3)
            function(*args)
            print("Después de funcion(*args)")
        return wrapped_function  # Con este objeto reemplazamos la función original

    return wrap  # Devolvemos el objeto que se ejecuta al llamar a la función decorada


@decorator_function_with_args("Aquí", "pasamos", "otros datos")
def test(arg1, arg2, arg3):
    print("Argumentos de test:", arg1, arg2, arg3)


print("Después de decorar")

print("\nLlamamos a función varias veces\n")
test("Hola", "Mundo!", "¿Qué tal?")
print("Despues de primera llamada\n")
test("Hello", "World!", "What's up?")
print("Despues de segunda llamada\n")
