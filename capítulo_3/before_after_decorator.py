class EjemploAntesDespues:

    def __init__(self, decorated_function):
        self.decorated_function = decorated_function  # El constructor almacena el argumento

    def __call__(self):
        print("\nAntes (before) de la ejecución de", self.decorated_function.__name__)
        # self.decorated_function.__name__ devuelve el nombre del argumento almacenado

        self.decorated_function()  # Ejecutamos el argumento almacenado
        print("Después (after) de la ejecución de", self.decorated_function.__name__)


@EjemploAntesDespues
def test1():
    print("Dentro de test1()")


@EjemploAntesDespues
def test2():
    print("Dentro de test2()")


test1()  # Llamamos a la función test1()
test2()  # Llamamos a la función test2()
