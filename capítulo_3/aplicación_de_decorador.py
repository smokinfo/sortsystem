class EjemploDeAnotacion:
    # El decorador-clase

    def __init__(self, decorated_function):
        # Ejecutamos el código de función __init__ al instanciar la función decorada

        print("Dentro de EjemploDeAnotacion.__init__")
        decorated_function()  # Ejecutamos la función decorada.

    def __call__(self):
        # Ejecutamos el código de función __call__ cuando llamamos a la función decorada

        print("Dentro de EjemploDeAnotacion.__call__")


@EjemploDeAnotacion
def foo():
    print("Dentro de foo")


print("Terminamos de decorar la función foo")
foo()  # Llamamos a la funcion


