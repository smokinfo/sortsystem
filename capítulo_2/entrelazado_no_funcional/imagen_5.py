class Gato:
    def __init__(self, nombre):
        self.nombre = nombre

    def miau(self):
        print(self.nombre, "dice MIAU!")


objeto_de_clase = Gato("Simba")
print("El nombre de gato es", objeto_de_clase.nombre)
objeto_de_clase.miau()
