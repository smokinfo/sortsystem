import logging


class SecurityException(object, Exception):
    pass


def sacar_dinero(id_de_cuenta, cantidad):

        logging.info("Cantidad de dinero de", id_de_cuenta, "es", cantidad)

        transaccion = empezar_transaccion()

        cuenta = Cuenta.obtener_cuenta(id_de_cuenta)
        saldo = cuenta.obtener_saldo()
        saldo = saldo - cantidad
        cuenta.poner_saldo(saldo)

        transaccion.terminar()






def tiene_acceso(v):
    pass


def empezar_transaccion():
    return 1


class Cuenta:
    def obtener_cuenta(v):
        return Cuenta

    @staticmethod
    def obtener_saldo():
        return 1

    @classmethod
    def poner_saldo(cls, saldo):
        pass