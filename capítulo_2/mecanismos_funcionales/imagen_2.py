def multiplicar_funcion_pura(array_de_numeros):
    retval = []
    for cada_numero in array_de_numeros:
        retval.append(cada_numero * 2)
    return retval


numeros_originales = [1, 2, 3, 4]
numeros_multiplicados = multiplicar_funcion_pura(numeros_originales)

print("numeros_originales:", numeros_originales)
print("numeros_multiplicados:", numeros_multiplicados)
