import logging


def higher_order_function(string, function):
    function(string)


higher_order_function("Print: ¡Hola Mundo!", print)
higher_order_function("Error: ¡Hola logging.error!", logging.error)
