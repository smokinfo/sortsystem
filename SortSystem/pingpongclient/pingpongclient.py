import socket
from pingpongdecoradores.cyphering_decorator_ping_pong import cyphering_decorator
from pingpongdecoradores.retrysort_decorator_ping_pong import RetrySortDecorator


class Client:
    def __init__(self):
        self.sort_service_port = 4444
        self.host = '127.0.0.1'
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.settimeout(5)

    @RetrySortDecorator
    def start(self):
        try:
            self.sock.connect((self.host, self.sort_service_port))
            print("\nThe server was sucessfully connected!")
        except Exception:
            return None
        shots = 1
        print("\nLet's play some Ping Pong with the server!")
        while shots != 6:
            self.send_message(self.sock, "PING!")
            print("\nRonda #{}".format(str(shots)))
            print("Client hits: PING!")
            data = self.receive_message(self.sock)
            print("Server replies:", data.decode())

            if shots == 5:
                print("\nThe game is over!")
                return True
            shots = shots + 1

    #@cyphering_decorator
    def send_message(self, connection, message):
        if isinstance(message, str):
            connection.send(message.encode())
        else:
            connection.send(message)

    #@cyphering_decorator
    def receive_message(self, connection):
        data = connection.recv(1024)
        return data


if __name__ == '__main__':
    Client.start(Client())
