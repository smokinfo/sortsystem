from cryptography.fernet import Fernet
from SortSystem.utils.auxiliar import prints as p
from decorators.decorator_configuration import DecoratorConfiguration


class Cyphering:
    key = b'oxt1xbr4J6EhPH8oEyShKtqWWvlhb34bNSRaQk9Bzyw='  # Default (Fernet) key

    def encrypt(self, message):
        f = Fernet(self.key)

        try:
            if isinstance(message, str):
                message = message.encode()
            else:
                message = bytes(message)

            fernet_token = f.encrypt(message)
            """
            encrypt(data)
            
            Encrypts data passed. The result of this encryption is known as a “Fernet token” 
            and has strong privacy and authenticity guarantees.
            
            "Fernet Token": https://docs.openstack.org/keystone/pike/admin/identity-fernet-token-faq.html
            
            :param data(bytes): The message you would like to encrypt.
            :returns bytes:  A secure message that cannot be read or altered without the key. 
                             It is URL-safe base64-encoded. This is referred to as a “Fernet token”.
            :raises: TypeError – This exception is raised if data is not bytes.
            """

        except TypeError:
            p.prints("[SymmetricCyphering.encrypt] Data is not 'bytes'!!")
            raise TypeError

        return fernet_token

    def decrypt(self, fernet_token):
        f = Fernet(self.key)

        try:
            if isinstance(fernet_token, list):
                print("[SymmetricCyphering] To the cypher came an array. "
                      "Check if you used the cyphering for both client and server. TURNING OFF THE CYPHERING")
                DecoratorConfiguration().cyphering_on = False
                return fernet_token

            message = f.decrypt(fernet_token)
            """
            Decrypts a Fernet token. If successfully decrypted you will receive the original plaintext as the result, 
            otherwise an exception will be raised. It is safe to use this data immediately as Fernet verifies 
            that the data has not been tampered with prior to returning it.
            """
            if message.__contains__(b'stop'):
                message = message.decode()
            else:
                message = list(message)

        except TypeError:
            p.prints("[SymmetricCyphering.decrypt] Token is not 'bytes'!!")
            raise TypeError

        return message

