import logging
import os
import time
from SortSystem.utils.auxiliar import prints as p


class MyLogger:
    """
    Logger static class
    """

    def __init__(self, name):
        self.name_log_file = ""
        self.has_been_initialized = False
        """
        This boolean states whether or not the logger has been initialized
        """
        self.name = name
        self.LOGGER = logging.getLogger(name)
        """
        Logger under use
        """

    def setup(self, name_log_file):
        """
        Method to set up an instance of the class. The provided name will be completed with the current time in order
        to produce a unique file name.

        :param name_log_file: Name for the log file. The final log file name will also include a time stamp
        to make the log file name unique.
        """
        if self.name_log_file == "":
            self.name_log_file = name_log_file

        root_logger = logging.getLogger()
        handlers = root_logger.handlers  # list
        if handlers:
            if isinstance(handlers[0], logging.StreamHandler):
                root_logger.removeHandler(logging.StreamHandler())  # suppress the logging output to the console

        dir_name = ""

        current_dir = os.path.dirname(  # SortSystem/SortSystem/auxiliar/logging
            os.path.dirname(  # SortSystem/SortSystem/auxiliar
                os.path.dirname(  # SortSystem/SortSystem
                    os.path.dirname(__file__)  # ROOT DIRECTORY: SortSystem/
                )
            )
        )

        if not os.path.exists("{}/logs".format(current_dir)):
            os.mkdir("{}/logs".format(current_dir))  # creates directory /logs in current_dir
            dir_name = "{}/logs/".format(current_dir)

        if os.path.exists("{}/logs".format(current_dir)):
            dir_name = "{}/logs/".format(current_dir)
        else:
            p.prints("[Logging.setup] No directory for logs available.")

        if self.name_log_file is not None:
            dir_name = dir_name + self.name_log_file
        else:
            dir_name = dir_name + "Logging"

        file_name = dir_name + "-" + str(int(round(time.time() * 1000)))

        # SETTING FILTER'S VARIABLES
        self.LOGGER.setLevel(logging.INFO)  # setting level

        class FormatterFilter(logging.Filter):
            """
            Inner class used to set custom field for Formatter
            :argument logging.Filter: is the Parent class of FormatterFilter
            """

            def filter(self, record):
                record.function_name = name_log_file  # Adding new fields to the formatter
                record.level = level  # Adding new fields to the formatter
                record.colorized = colorizing_level

                return True

        if self.LOGGER.level >= logging.WARNING:  # colorize any levels >= WARNING in red

            if self.LOGGER.level == logging.WARNING:  # setting WARNING name
                level = "WARNING"
            elif self.LOGGER.level == logging.ERROR:  # setting ERROR name
                level = "ERROR"
            else:  # setting CRITICAL name
                level = "CRITICAL"

            colorizing_level = "<td style=\"color:red\">"

        else:  # If there is no need to colorize levels

            if self.LOGGER.level == logging.DEBUG:  # setting DEBUG name
                level = "DEBUG"
            else:  # setting INFO name
                level = "INFO"

            colorizing_level = "<td align=\"Loglevel\">"

        self.LOGGER.addFilter(FormatterFilter())  # adding new fields
        file_txt = logging.FileHandler(file_name + ".txt", "w", encoding='UTF-8')  # creating new .txt file
        file_html = logging.FileHandler(file_name + ".html", "w", encoding='UTF-16')

        formatter_txt = logging.Formatter(  # Formatter txt
            '%(asctime)s - [MAIN function: %(function_name)s] \n%(levelname)s: %(message)s\n'
        )

        formatter_html = logging.Formatter(
            """
<html>
    <head>
    <!DOCTYPE html>
            <style>
                table {width: 100%%}
                th {font:bold 10pt Tahoma;}
                td {font:normal 10pt Tahoma;}
                h1 {font:normal 11pt Tahoma;}
            </style>
        <body>
            <table border="0" cellpadding="5" cellspacing="3">
                <tr align="center">
                    <th style="width:10%%">Loglevel</th>
                    <th style="width:40%%">Time</th>
                    <th style="width:50%%">Log Message</th>
                </tr>
                <tr>
                    %(colorized)s
                        %(level)s:
                    </td>
                    <td align="center">
                       %(asctime)s
                    </td>
                    <td align="right">
                        %(message)s
                    </td>
                </tr>
            </table>
        </body>
</html>
"""
        )

        file_txt.setFormatter(formatter_txt)  # setting formatter for txt
        file_html.setFormatter(formatter_html)  # setting formatter for txt
        self.LOGGER.addHandler(file_txt)  # adding .txt handler
        self.LOGGER.addHandler(file_html)  # adding .html handler
