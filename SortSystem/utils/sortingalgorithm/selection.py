from SortSystem.utils.sortingalgorithm.sortingalgorithm import SortingAlgorithm


class SelectionSort(SortingAlgorithm):
    def sort(self, data):
        for i in range(len(data)):

            # Find the minimum element in remaining
            # unsorted array
            min_index = i
            for j in range(i + 1, len(data)):
                if data[min_index] > data[j]:
                    min_index = j

            # Swap the found minimum element with
            # the first element
            data[i], data[min_index] = data[min_index], data[i]

        return data
