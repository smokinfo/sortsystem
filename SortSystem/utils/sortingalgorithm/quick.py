from SortSystem.utils.sortingalgorithm.sortingalgorithm import SortingAlgorithm


def quick_sort_helper(data, first, last):
    if first < last:
        split_point = partition(data, first, last)

        quick_sort_helper(data, first, split_point - 1)
        quick_sort_helper(data, split_point + 1, last)
    return data


def partition(data, first, last):
    pivot_value = data[first]

    left = first + 1
    right = last

    done = False
    while not done:

        while left <= right and data[left] <= pivot_value:
            left = left + 1

        while data[right] >= pivot_value and right >= left:
            right = right - 1

        if right < left:
            done = True
        else:
            temp = data[left]
            data[left] = data[right]
            data[right] = temp

    temp = data[first]
    data[first] = data[right]
    data[right] = temp

    return right


class QuickSort(SortingAlgorithm):
    def sort(self, data):
        return quick_sort_helper(data, 0, len(data) - 1)
