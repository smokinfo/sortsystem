from SortSystem.utils.sortingalgorithm.sortingalgorithm import SortingAlgorithm


def heapify(arr, n, i):
    largest = i
    k = 2 * i + 1
    r = 2 * i + 2

    # See if left child of root exists and is
    # greater than root
    if k < n and arr[i] < arr[k]:
        largest = k

    # See if right child of root exists and is
    # greater than root
    if r < n and arr[largest] < arr[r]:
        largest = r

    # Change root, if needed
    if largest != i:
        arr[i], arr[largest] = arr[largest], arr[i]

        # Heapify the root.
        heapify(arr, n, largest)


class HeapSort(SortingAlgorithm):
    def sort(self, arr):
        n = len(arr)

        for i in range(n, -1, -1):
            heapify(arr, n, i)

        for i in range(n - 1, 0, -1):
            arr[i], arr[0] = arr[0], arr[i]
            heapify(arr, i, 0)

        return arr
