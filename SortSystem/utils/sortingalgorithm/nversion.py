import copy
from concurrent.futures import ThreadPoolExecutor
from utils.sortingalgorithm.sortingalgorithm import SortingAlgorithm
from utils.sortingalgorithm.sortingalgorithmfactory import SortingAlgorithmFactory, alg_type
from utils.auxiliar import prints as p


class NVersionSort(SortingAlgorithm):
    def __init__(self, algorithms=None):
        self.data = []
        self.versions = []

        if isinstance(algorithms, list):
            # Checking if the user pass an array...
            self.algorithms = algorithms

        elif algorithms is None:
            # ...or nothing...
            self.algorithms = [SortingAlgorithmFactory.new_sa(alg_type.QUICKSORT),
                               SortingAlgorithmFactory.new_sa(alg_type.HEAPSORT),
                               SortingAlgorithmFactory.new_sa(alg_type.MERGESORT),
                               SortingAlgorithmFactory.new_sa(alg_type.BUBBLESORT),
                               SortingAlgorithmFactory.new_sa(alg_type.SELECTIONSORT),
                               SortingAlgorithmFactory.new_sa(alg_type.INSERTIONSORT),
                               SortingAlgorithmFactory.new_sa(alg_type.PYTHONARRAYSORT)]

        else:
            # ... or an algorithm.
            self.algorithms.append(algorithms)

        self.versions = self.__setitem__(self.algorithms)

    def __setitem__(self, algorithm):
        """
        Setter of the various instantiated algorithms
        """
        if algorithm is None or len(algorithm) == 0:
            return

        for i in range(0, len(self.algorithms)):
            # Any sort coming. For example quick sort.
            # algorithm: <auxiliar.sortingalgorithm.quick_sort.QuickSort object at 0x0074E508>

            nvt = NVersionSort.NVersionThread(self.algorithms[i])
            # nvt: <__main__.NVersionSort.NVersionThread object at 0x026DDAC0>

            self.versions.append(nvt)
        return self.versions

    def __getitem__(self):
        """
        Getter for checking the available versions (null if no version is available)

        :return: An array containing the various SortingAlgorithm versions under use
        """
        if self.versions is None or len(self.versions) == 0:
            return None

        return_sa = []

        for i in range(len(self.versions)):
            return_sa.append(self.versions[i].algorithm)
        return return_sa

    def sort(self, array):
        if self.algorithms is None or len(self.algorithms) == 0:
            p.prints("[NVersionSort.sort] No versions available")
            return None

        p.prints("[NVersionSort.sort] Working with {} versions".format(len(self.algorithms)))
        thread = ThreadPoolExecutor()
        p.prints("[NVersionSort.sort] Transmitting the array to sort to all versions")

        for i in range(len(self.algorithms)):
            self.versions[i].__setitem__(array)
            p.prints("[NVersionThread.__setitem__] Cloning the data...")

        if len(self.versions) == 1:
            return self.versions[0].__getitem__()

        p.prints("[NVersionSort.sort] All versions are now under execution")
        p.prints("[NVersionSort.sort] Waiting for all versions to finish")

        for i in range(len(self.algorithms)):
            future = thread.submit(self.versions[i].run)
            d = future.result()
            self.versions[i].__setitem__(d)

        for i in range(len(self.algorithms)):
            thread.shutdown(wait=True)

        p.prints("[NVersionSort.sort] Voting for the major result")
        data = self.majority_vote()
        return data

    def majority_vote(self):
        arr_list = []

        for i in range(len(self.versions)):
            index = self.index_result_list(arr_list, self.versions[i].__getitem__())
            if index == -1:
                arr_list.append(NVersionSort.ResultMatches(self.versions[i].__getitem__(), i))
            else:
                arr_list[index].matching_indexes.append(i)

        index = self.index_most_popular_option_in_list(arr_list)
        if index == -1:
            return None
        else:
            if len(arr_list[index].matching_indexes) >= ((int((len(self.versions))/2))+1):
                if arr_list[index].result is None:
                    return None
                else:
                    return copy.copy(arr_list[index].result)
            else:
                return None

    @staticmethod
    def index_most_popular_option_in_list(arr_list):
        if len(arr_list) == 1:
            return 0
        most_popular_index = 0
        for i in range(len(arr_list)):
            if len(arr_list[most_popular_index].matching_indexes) < len(arr_list[i].matching_indexes):
                most_popular_index = i
        return most_popular_index

    @staticmethod
    def index_result_list(arr, result):
        if len(arr) == 0:
            return -1
        for i in range(len(arr)):
            rm = arr[i]
            if result == rm.result:
                return i
        return -1

    class ResultMatches:
        def __init__(self, result, i):
            self.result = copy.copy(result)
            self.matching_indexes = []
            self.matching_indexes.append(i)

    class NVersionThread:
        """
        This inner class is the thread carrying out the sorting of the provided array of ints
        """

        def __init__(self, algorithm):
            self.algorithm = algorithm
            self.data = []

        def __setitem__(self, value):
            if self.algorithm is not None:
                self.data = copy.copy(value)

        def __getitem__(self):
            if self.algorithm is not None:
                return copy.copy(self.data)

        def run(self):
            if self.algorithm is not None:
                return self.algorithm.sort(self.__getitem__())
