from SortSystem.utils.sortingalgorithm.sortingalgorithm import SortingAlgorithm


class PythonArraySort(SortingAlgorithm):
    def sort(self, value):
        return sorted(value)
