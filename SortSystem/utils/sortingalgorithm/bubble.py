from SortSystem.utils.sortingalgorithm.sortingalgorithm import SortingAlgorithm


class BubbleSort(SortingAlgorithm):
    def sort(self, data):  # data = int[]
        n = len(data)
        for i in range(n-1):
            for j in range(0, n-i-1):
                if data[j] > data[j+1]:
                    data[j], data[j+1] = data[j+1], data[j]
        return data
