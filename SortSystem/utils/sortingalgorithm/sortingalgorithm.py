class SortingAlgorithm:

    def sort(self, data):
        pass

    class Type:
        QUICKSORT = "QUICKSORT"
        MERGESORT = "MERGESORT"
        HEAPSORT = "HEAPSORT"
        BUBBLESORT = "BUBBLESORT"
        SELECTIONSORT = "SELECTIONSORT"
        INSERTIONSORT = "INSERTIONSORT"
        PYTHONARRAYSORT = "PYTHONARRAYSORT"
        NVERSIONSORT = "NVERSIONSORT"

        def __getitem__(self):
            versions = [self.QUICKSORT, self.MERGESORT, self.HEAPSORT, self.BUBBLESORT,
                        self.SELECTIONSORT, self.INSERTIONSORT, self.PYTHONARRAYSORT, self.NVERSIONSORT]
            return versions
