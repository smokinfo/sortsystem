from utils.sortingalgorithm.sortingalgorithm import SortingAlgorithm

alg_type = SortingAlgorithm.Type


class SortingAlgorithmFactory:

    @staticmethod
    def new_sa(t):
        if t == alg_type.QUICKSORT:
            from utils.sortingalgorithm.quick import QuickSort
            return QuickSort()

        elif t == alg_type.MERGESORT:
            from utils.sortingalgorithm.merge import MergeSort
            return MergeSort()

        elif t == alg_type.HEAPSORT:
            from utils.sortingalgorithm.heap import HeapSort
            return HeapSort()

        elif t == alg_type.BUBBLESORT:
            from utils.sortingalgorithm.bubble import BubbleSort
            return BubbleSort()

        elif t == alg_type.SELECTIONSORT:
            from utils.sortingalgorithm.selection import SelectionSort
            return SelectionSort()

        elif t == alg_type.INSERTIONSORT:
            from utils.sortingalgorithm.insertion import InsertionSort
            return InsertionSort()

        elif t == alg_type.PYTHONARRAYSORT:
            from utils.sortingalgorithm.pythonarray import PythonArraySort
            return PythonArraySort()

        elif t == alg_type.NVERSIONSORT:
            from utils.sortingalgorithm.nversion import NVersionSort
            return NVersionSort()

