from SortSystem.utils.sortingalgorithm.sortingalgorithm import SortingAlgorithm


class MergeSort(SortingAlgorithm):
    def sort(self, data):
        if len(data) > 1:
            mid = len(data) // 2
            left = data[:mid]
            right = data[mid:]

            self.sort(left)
            self.sort(right)

            i = j = temp = 0

            # Copy data to temp arrays left[] and right[]
            while i < len(left) and j < len(right):
                if left[i] < right[j]:
                    data[temp] = left[i]
                    i += 1
                else:
                    data[temp] = right[j]
                    j += 1
                temp += 1

            # Checking if any element was left
            while i < len(left):
                data[temp] = left[i]
                i += 1
                temp += 1

            while j < len(right):
                data[temp] = right[j]
                j += 1
                temp += 1

        return data
