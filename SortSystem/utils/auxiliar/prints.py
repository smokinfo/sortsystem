from decorators.logging_decorator import LoggingDecorator


@LoggingDecorator
def prints(string):
    print(string)
