import random


def random_boolean():
    return random.random < random.random

def random_integer_bound(bound):
    if bound == 0:
        return bound

    else:
        return random.randrange(bound)


def random_data_array(number_of_elements_bound, max_value):
    number_of_elements = 0

    while number_of_elements == 0:
        number_of_elements = random.randrange(number_of_elements_bound)

    return_data = []

    for i in range(number_of_elements):
        x = random.randrange(max_value)
        return_data.append(x)

    return return_data
