import json
from json import JSONDecodeError

from decorators.cyphering_decorator import cyphering_decorator


class SocketCommunication:
    def __init__(self, s):
        self.socket = s

    @cyphering_decorator("read")
    def read_message_from_socket(self, tag):
        data = self.socket.recv(4096)
        try:
            data = json.loads(data)
            # json.loads: Deserialize s (a str, bytes or bytearray instance containing a JSON document) to a Python...
            # ...object using special conversion table.

            message = data.get(tag)
            return message
        except JSONDecodeError:
            return data

    @cyphering_decorator("write")
    def write_message_to_socket(self, tag, message):
        if isinstance(message, bytes):
            self.socket.send(message)
        else:
            data = json.dumps({"{}".format(tag): message})
            # json.dumps: Serialize obj to a JSON formatted str using special conversion table.
    
            self.socket.send(data.encode())

