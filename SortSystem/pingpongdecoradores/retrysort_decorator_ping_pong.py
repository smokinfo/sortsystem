import time


class RetrySortDecorator:
    def __init__(self, function):
        self.function = function
        self.ret_data = None

    def __call__(self, *args):

        retries = 0

        while retries < 9:
            self.ret_data = self.function(*args)

            if self.ret_data is not None:
                break
            else:
                print("\nRetry({}) again in {} secs.".format(retries + 1, 2 * (retries + 1)))
                retries = retries + 1
                time.sleep(2 * (retries + 1))
