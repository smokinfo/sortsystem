from cryptography.fernet import Fernet


def cyphering_decorator(func):
    key = b'oxt1xbr4J6EhPH8oEyShKtqWWvlhb34bNSRaQk9Bzyw='
    f = Fernet(key)

    def wrapper(*args):
        if func.__name__ == "send_message":
            print("Message to encrypt:", args[2])
            encrypted_message = f.encrypt(args[2].encode())
            encrypted_message = func(args[0], args[1], encrypted_message)
            return encrypted_message

        elif func.__name__ == "receive_message":
            encrypted_message = func(*args)
            print("Message to decrypt:", encrypted_message)

            decrypted_message = f.decrypt(encrypted_message)
            return decrypted_message

    return wrapper

