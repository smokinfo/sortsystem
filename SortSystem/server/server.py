from concurrent.futures import ThreadPoolExecutor
import socket
from SortSystem.server.serverthread import ServerThread
from decorators.backup_decorator import BackupDecorator
from utils.sortingalgorithm.sortingalgorithmfactory import SortingAlgorithmFactory, alg_type
from SortSystem.utils.auxiliar import prints as p
from utils.auxiliar.helper import Helper


class Server:
    def __init__(self):
        self.port = 11111  # Número de puerto que usamos
        self.client_number = 1  # El número del cliente que hace petición al servidor
        self.host = '127.0.0.1'  # El nombre de host (127.0.0.1 por defecto)
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # Instanciamos el socket
        self.server.bind((self.host, self.port))  # Asignamos al socket el host y el puerto
        self.server.listen(10)  # Escuchamos a clientes

    def set_client_number(self, client_number):  # El setter para el número del cliente
        @BackupDecorator("set", client_number)
        def init_set_client_number(client_n=client_number):
            self.client_number = client_n

    def get_client_number(self):  # El getter para el número del cliente
        @BackupDecorator("get")
        def init_get_client_number():
            return self.client_number
        return init_get_client_number

    def server_start(self):
        client_number = self.get_client_number()
        executor = ThreadPoolExecutor()
        p.prints("> [Server.start] Hi!!! Starting the sorting server at port: {}".format(self.port))
        while Helper.stop is not True:

            if Helper.stop is True:
                break

            p.prints("> [Server.start] Waiting for a new client ...")
            conn, addr = self.server.accept()
            p.prints("> [Server.start] Processing request for client #{}...".format(client_number))
            self.set_client_number(client_number + 1)

            sst = ServerThread(conn, SortingAlgorithmFactory.new_sa(alg_type.BUBBLESORT))
            p.prints(">> Serving the client in a thread ...")
            future = executor.submit(sst.run, conn)
            future.result()

        p.prints("> [Server.start] Server is out of the do while loop ... bye!!!")
        executor.shutdown()
        p.prints("> [Server.start] All threads in the pool terminated ... bye!!!")
        self.stop_server()

    def stop_server(self):
        p.prints("> [Server.stopServer] Stopping the server ...")
        if self.server is not None:
            p.prints("> [Server.stopServer] Closing the socket ...")
            self.server.close()
        p.prints("> [Server.stopServer] Setting stop variable to true ...")

        print("Presione enter para continuar . . .")

        exit_button = input()
        if exit_button is not None:
            exit(0)


if __name__ == '__main__':
    Server().server_start()