from SortSystem.server.serverproxy import ServerProxy
from utils.auxiliar import prints as p


class ServerStop:
    @staticmethod
    def main():
        p.prints(">> Stopping the server ...")
        try:
            ServerProxy().stop()
        except ConnectionRefusedError:
            pass
        p.prints(">> Done.")
