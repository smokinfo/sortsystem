import copy
import socket
from SortSystem.utils.auxiliar import prints as p
from utils.comlib.socketcommunication import SocketCommunication
from decorators.retrysort_decorator import RetrySortDecorator
from utils.auxiliar.strings import Strings


class ServerProxy:
    def __init__(self):
        self.sort_service_port = 11111
        self.host = '127.0.0.1'
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    @RetrySortDecorator
    def sort(self, array):
        try:
            self.sock.connect((self.host, self.sort_service_port))
            com_channel = SocketCommunication(self.sock)
            p.prints("[ServerProxy.sort] Writing data to socket")
            com_channel.write_message_to_socket(Strings.from_client_to_server, array)
            p.prints("[ServerProxy.sort] Reading data from socket")
            return_data = com_channel.read_message_from_socket(Strings.from_server_to_client)

        except Exception as ex:
            p.prints("[ServerProxy.sort] Connection problems. Check whether the server is down !!!")
            p.prints("[ServerProxy.sort] Exception received: {}".format(ex))
            return

        if return_data is None:
            p.prints("[ServerProxy.sort] return_data is None.")
            return

        else:
            data_to_sort = copy.copy(return_data)
            return data_to_sort

    def stop(self):
        try:
            self.sock.connect((self.host, self.sort_service_port))
            com_channel = SocketCommunication(self.sock)
            com_channel.write_message_to_socket(Strings.from_client_to_server, Strings.stop)
            if self.sock is not None:
                self.sock.close()

        except IOError as ex:
            p.prints("[ServerProxy.sort] Connection problems. Check whether the server is down !!!")
            p.prints("[ServerProxy.sort] Exception received \"{}\": {}".format(ConnectionRefusedError.__name__, ex))
            return
