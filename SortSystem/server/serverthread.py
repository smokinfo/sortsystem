import socket

from decorators.nversion_decorator import NVersionDecorator
from utils.comlib.socketcommunication import SocketCommunication
from utils.auxiliar.helper import Helper
from utils.auxiliar.strings import Strings
from SortSystem.utils.auxiliar import prints as p


class ServerThread:
    def __init__(self, client, algorithm):
        self.client = client
        self.algorithm = algorithm

    def run(self, conn):
        communication = SocketCommunication(conn)
        try:
            p.prints(">> Reading data to sort ...")
            message = communication.read_message_from_socket(Strings.from_client_to_server)
            if message == "stop":
                Helper.stop = True
                p.prints(">> Received stop order ... \n>> Stopping the server execution ...")
                conn.close()

            if message is not None:
                p.prints(">> This is the array to sort (server view) : {}".format(message))
                if self.algorithm is not None:

                    @NVersionDecorator
                    def get_data(data_to_sort=message):
                        ret_data = self.algorithm.sort(data_to_sort)
                        return ret_data

                    data = get_data(message)

                    p.prints(">> This is the sorted array (server view) : {}".format(data))
                    p.prints(">> Now writing the sorted array to the socket.")
                    communication.write_message_to_socket(Strings.from_server_to_client, data)
                else:
                    p.prints(">> Unable to sort because there is a problem with the algorithm to use")
            else:
                p.prints(">> [ServerThread] Problems when reading message from socket ...")
        except socket.error:
            conn.close()
            raise socket.error("Socket error in ServerThread.run()")
        conn.close()


