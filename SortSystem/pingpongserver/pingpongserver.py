import socket
from pingpongdecoradores.cyphering_decorator_ping_pong import cyphering_decorator


class PingPongServer:
    def __init__(self):
        self.port = 4444
        self.host = '127.0.0.1'
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.bind((self.host, self.port))
        self.server.listen(10)

    def start(self):
        print("\nLet's play some Ping Pong with the client!")
        shots = 1
        conn, addr = self.server.accept()
        while shots != 6:
            data = self.receive_message(conn)
            print("\nRonda #{}".format(str(shots)))
            print("Client attacks:", data.decode())
            print("Server has an answer: PONG!")
            self.send_message(conn, "PONG!")
            if shots == 5:
                print("\nThe game is over!")
                break
            shots = shots + 1

    #@cyphering_decorator
    def send_message(self, connection, message):
        if isinstance(message, str):
            connection.send(message.encode())
        else:
            connection.send(message)

    #@cyphering_decorator
    def receive_message(self, connection):
        data = connection.recv(1024)
        return data


if __name__ == '__main__':
    PingPongServer.start(PingPongServer())
