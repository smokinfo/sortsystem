from decorators.nversion_decorator import NVersionDecorator
from decorators.proxy_decorator import ProxyDecorator
from decorators.retrysort_decorator import RetrySortDecorator
from utils.randomgen import randomizer
from SortSystem.utils.auxiliar import prints as p

from utils.sortingalgorithm.sortingalgorithmfactory import SortingAlgorithmFactory, alg_type


class Client:

    #@RetrySortDecorator
    @ProxyDecorator
    def sort(self, args):
        return SortingAlgorithmFactory.new_sa(alg_type.BUBBLESORT).sort(args)

    def main(self):
        data = randomizer.random_data_array(10, 20)
        p.prints("> [Client.main] This is the array to sort (client view): {}".format(data))
        sort_data = self.sort(data)

        if sort_data is None:
            p.prints("> [Client.main] The sorted array returned None. Check whether the server is down !!!")
        else:
            p.prints("> [Client.main] This is the sorted array (client view): {} \n".format(sort_data))


if __name__ == '__main__':
    Client().main()
