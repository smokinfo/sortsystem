class DecoratorConfiguration:
    proxy_on = False
    logging_on = False
    cyphering_on = False  # Server-client side
    FTRetry_on = False  # Client side
    FTBackup_on = False  # Server side
    FTDiversification_on = False
