import os
from functools import wraps
from SortSystem.utils.auxiliar import prints as p
from decorators.decorator_configuration import DecoratorConfiguration


class BackupDecorator:
    def __init__(self, type_of_func=None, args=None):
        self.args = args
        self.type_of_func = type_of_func

    def __call__(self, func):
        @wraps(func)
        def wrapper():

            dir_name = get_dir_name()

            if self.type_of_func == "set":
                saved_args = self.args
                file = open(dir_name + "saved_args", "w", encoding="UTF-8")
                saved_args = str(saved_args)
                file.write(saved_args)
                func(self.args)

            elif self.type_of_func == "get":
                try:
                    file = open(dir_name + "saved_args", "r", encoding="UTF-8")
                    data = int(file.read())
                    print("[BackupDecorator] The previous state was loaded. Continuing the execution.")
                    return data

                except FileNotFoundError:
                    file = open(dir_name + "saved_args", "w", encoding="UTF-8")
                    file.write('0')
                    file.close()
                    file = open(dir_name + "saved_args", "r", encoding="UTF-8")
                    data = int(file.read())
                    return data

        if DecoratorConfiguration.FTBackup_on is True:
            return wrapper()
        else:
            if self.type_of_func == "set":
                func(self.args)
            elif self.type_of_func == "get":
                return func()


def get_dir_name():
    current_dir = os.path.dirname(  # SortSystem/SortSystem/auxiliar/logging
        os.path.dirname(  # SortSystem/SortSystem/auxiliar
            os.path.dirname(__file__)  # ROOT DIRECTORY: SortSystem/
        )
    )

    if not os.path.exists("{}/backups".format(current_dir)):
        os.mkdir("{}/backups".format(current_dir))  # creates directory /logs in current_dir
        dir_name = "{}/".format(current_dir)
        return dir_name

    if os.path.exists("{}/backups".format(current_dir)):
        dir_name = "{}/backups/".format(current_dir)
        return dir_name
    else:
        p.prints("[BackupDecorator.setup] No directory for backups available.")


def save_options(name, args):
    file = open(get_dir_name() + name, "w", encoding="UTF-8")
    file.write(args)
    file.close()


def get_options(name):
    try:
        file = open(get_dir_name() + name, "r", encoding="UTF-8")
        options = file.read()
        file.close()
    except (OSError, IOError):
        print("\n[BackupDecorator.setup] No such file is available.")
        return None
    return options
