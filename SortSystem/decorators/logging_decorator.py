import inspect
import logging
import os

from decorators.decorator_configuration import DecoratorConfiguration
from utils.logging.my_logger import MyLogger
from utils.auxiliar.helper import Helper


class LoggingDecorator:
    def __init__(self, function):
        self.function = function

    def __call__(self, *args):
        if DecoratorConfiguration().logging_on is True:
            logger_tag = "logging"
            name_log_file = get_name_for_log_file()

            if not Helper.initialized:  # Checking if the logger has already been initiated.
                if DecoratorConfiguration().logging_on is True:
                    MyLogger(logger_tag).setup(name_log_file)  # Setting up the logger
                    Helper.initialized = True  # Changing helping variable to avoid multiple instantiation

            logger = logging.getLogger(logger_tag)
            if not logger:
                raise Exception("Logger does not exist. Please, check logging decorator")
            else:
                self.function(*args)
                logger.info(*args)
        else:
            self.function(*args)


def get_name_for_log_file():

    frame = inspect.stack()[1]                       # stack or frame to get func name
    function_name = frame.function                   # getting function name
    path = str(frame[0])                             # getting caller's name
    filename = os.path.basename(path).split('.')[0]  # removing extensions and unused info
    name_log_file = "{}.{}".format(                  # setting a filename and the name of a func to use it...
        filename,                                    # ...as a name of the logs
        function_name
    )

    return name_log_file


def save_options(name, args):
    dir_name = get_name_for_log_file()
    print("save_option.dir_name:", str(dir_name))
    if dir_name is None:
        raise ValueError
    else:
        file = open(dir_name + name, "w", encoding="UTF-8")
        print("save_option.file_name:", str(file))
        file.write(args)
        file.close()


def get_options(name):
    try:
        dir_name = get_name_for_log_file()
        print("get_options.dir_name:", str(dir_name))
        if dir_name is None:
            raise ValueError
        else:
            file = open(dir_name + name, "r", encoding="UTF-8")
            print("get_options.file_name:", str(file))
        options = file.read()
        file.close()
    except (OSError, IOError):
        print("\n[BackupDecorator.setup] No such file is available.")
        return None
    return options
