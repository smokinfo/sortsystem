from cyphering.cyphering import Cyphering
from decorators.decorator_configuration import DecoratorConfiguration


def cyphering_decorator(decorator_argument):
    def decorator(function):
        def wrapper(*args, **kwargs):
            if DecoratorConfiguration.cyphering_on is False:
                return function(*args, **kwargs)
            else:
                if decorator_argument == "write":
                    encrypted_message = Cyphering().encrypt(args[2])
                    print(">> Coding the message...")
                    encrypted_message = function(args[0], args[1], encrypted_message)
                    return encrypted_message

                elif decorator_argument == "read":
                    encrypted_message = function(*args, **kwargs)
                    decrypted_message = Cyphering().decrypt(encrypted_message)
                    print(">> Decoding the message...")
                    return decrypted_message
        return wrapper
    return decorator

