from decorators.decorator_configuration import DecoratorConfiguration
from server.serverproxy import ServerProxy


class ProxyDecorator:
    """
    It enables a seamless connection between a client class and a SortingServer
    """

    def __init__(self, function):
        self.function = function

    def __call__(self, *args):
        if DecoratorConfiguration().proxy_on is True:
            sorted_array = ServerProxy.sort(ServerProxy(), *args)
            return sorted_array
        else:
            return self.function(self, *args)

