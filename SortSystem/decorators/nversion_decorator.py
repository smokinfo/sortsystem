import copy
import traceback
from decorators.decorator_configuration import DecoratorConfiguration
from utils.sortingalgorithm.sortingalgorithmfactory import SortingAlgorithmFactory, alg_type


class NVersionDecorator:
    def __init__(self, function):
        self.function = function

    def __call__(self,  *args, **kw):
        if DecoratorConfiguration().FTDiversification_on is True:
            nv = SortingAlgorithmFactory.new_sa(alg_type.NVERSIONSORT)
            try:
                if args is None:
                    print("[NVersionDecorator.wrapper] Trying to sort a None type array of ints")
                    raise TypeError("NVersionDecorator recieved a None type array")

                else:
                    print("[NVersionDecorator.wrapper] Sorting array {}".format(*args))
                    if nv is not None:
                        output_data = nv.sort(*args)

                    else:
                        output_data = None
                    if output_data is None:
                        print(">> Hey a SORTING ERROR occured, please do your algorithm more Fault-Tolerant!!!")
            except Exception as ex:
                print(traceback.format_exc())
                raise ex

            if output_data is None:
                return None

            else:
                return copy.copy(output_data)
        else:
            return self.function(*args)
