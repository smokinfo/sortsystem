import time
from decorators.decorator_configuration import DecoratorConfiguration
from utils.auxiliar import prints as p


class RetrySortDecorator:
    def __init__(self, function):
        self.function = function
        self.ret_data = []

    def __call__(self, *args):
        if DecoratorConfiguration().FTRetry_on is True:
            retries = 0
            while retries < 9:
                self.ret_data = self.function(*args)
                if self.ret_data is not None:
                    p.prints("[RetrySortDecorator] The server is online. Continuing to sort the array...")
                    return self.ret_data
                else:
                    p.prints("[RetrySortDecorator] The server is not online. "
                             "Retry({}) again in {} secs \n".format(retries + 1, 2 * (retries + 1)))
                    retries = retries + 1
                    try:
                        time.sleep(2 * (retries + 1))
                    except Exception as e:
                        p.prints("[RetrySortDecorator] Sleeping problems {}, traceback: {}"
                                 .format(e.__cause__, e.__traceback__))
                        continue
            if retries == 9:
                p.prints("[RetrySortDecorator] Couldn't connect to the server. Please, restart the system.\n")
        else:
            return self.function(*args)
