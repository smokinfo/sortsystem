import cmd
import os
from SortSystem.utils.auxiliar import prints as p

import pyfiglet

# Outside library pyfiglet
from client.client import Client
from decorators.backup_decorator import save_options
from decorators.backup_decorator import get_options
from decorators.decorator_configuration import DecoratorConfiguration
from server.server import Server
from server.serverstop import ServerStop


class OptionsParser(cmd.Cmd):
    started = False

    args_to_save = ""

    prompt = "Enter your choice [1,2,3,4]? "

    def do_1(self, line):
        print("\n**********************************************************************************************")
        print("""* Select the options to use for the execution of the service:
*
*     -l : activates its LoggingAspect
*     -R : activates its ProxyAspect
*     -c : activates its CypheringAspect
*     -ft all,d,b,r : fault tolerance options
*                     d - Diversification (server-side)
*                     b - Backup (server-side)
*                     r - Retry (client-side)
*                     all - All applicable options (client/server side)
*     -file fName : provides the name of a file containing the options to use""")

        args_input = str(input())

        print("\n**********************************************************************************************")
        p.prints("[OptionsParser] Provided arguments are: {}".format(args_input))
        print("**********************************************************************************************\n")

        self.check_string(args_input, "server")

        os.system("start option_parser.py")
        Server().server_start()

    def do_2(self, line):
        print("\n**********************************************************************************************")
        print("""* Select the options to use for the execution of the service:
*
*     -l : activates its LoggingAspect
*     -R : activates its ProxyAspect
*     -c : activates its CypheringAspect (server-client)
*     -ft all,d,b,r : fault tolerance options
*                     d - Diversification (server-side)
*                     b - Backup (server-side)
*                     r - Retry (client-side)
*                     all - All applicable options (client/server side)
*     -file fName : provides the name of a file containing the options to use""")

        args_input = str(input())

        print("\n**********************************************************************************************")
        p.prints("[OptionsParser] Provided arguments are: {}".format(args_input))
        print("**********************************************************************************************\n")

        self.check_string(args_input, "client")

        Client().main()

        print("Presione enter para continuar . . .")
        exit_button = input()
        if exit_button is not None:
            exit(0)

    def do_3(self, line):
        ServerStop().main()
        print("Presione enter para continuar . . .")
        exit_button = input()
        if exit_button is not None:
            exit(0)

    def do_4(self, line):
        exit(0)

    def check_string(self, args, tag):
        if '-l' in args and not None:
            p.prints("[AspectConfiguration] LoggerON is true")
            self.args_to_save = self.args_to_save + '-l'
            DecoratorConfiguration.logging_on = True

        if '-R' in args and '-R'.isupper() and tag == "client" and not None:
            p.prints("[AspectConfiguration] ProxyON is true")
            self.args_to_save = self.args_to_save + '-R'
            DecoratorConfiguration.proxy_on = True

        if '-c' in args and not None:
            p.prints("[AspectConfiguration] CyphererON is true")
            self.args_to_save = self.args_to_save + '-c'
            DecoratorConfiguration.cyphering_on = True

        if '-ft d' in args and not None:
            p.prints("[AspectConfiguration] FaultToleranceON is true")
            p.prints("[AspectConfiguration] DiversificationON is true")
            self.args_to_save = self.args_to_save + '-ft d'
            DecoratorConfiguration.FTDiversification_on = True

        if '-ft r' in args and tag == "client" and not None:
            p.prints("[AspectConfiguration] FaultToleranceON is true")
            p.prints("[AspectConfiguration] RetryON is true")
            self.args_to_save = self.args_to_save + '-ft r'
            DecoratorConfiguration.FTRetry_on = True

        if '-ft d r' in args or '-ft r d' in args and not None:
            p.prints("[AspectConfiguration] FaultToleranceON is true")
            p.prints("[AspectConfiguration] DiversificationON is true")
            p.prints("[AspectConfiguration] RetryON is true")
            self.args_to_save = self.args_to_save + '-ft d r'
            DecoratorConfiguration.FTRetry_on = True
            DecoratorConfiguration.FTDiversification_on = True

        if '-ft all' in args and not None:
            p.prints("[AspectConfiguration] FaultToleranceON is true")
            if tag == "server":
                p.prints("[AspectConfiguration] DiversificationON is true")
                p.prints("[AspectConfiguration] BackupON is true")
                DecoratorConfiguration.FTDiversification_on = True
                DecoratorConfiguration.FTBackup_on = True
            elif tag == "client":
                p.prints("[AspectConfiguration] RetryON is true")
                DecoratorConfiguration.FTRetry_on = True

            self.args_to_save = self.args_to_save + '-ft all'

        if '-ft b' in args and tag == "server" and not None:
            p.prints("[AspectConfiguration] FaultToleranceON is true")
            p.prints("[AspectConfiguration] BackupON is true")
            self.args_to_save = self.args_to_save + '-ft b'
            DecoratorConfiguration.FTBackup_on = True

        if '-ft d b' in args or '-ft b d' in args and tag == "server" and not None:
            p.prints("[AspectConfiguration] FaultToleranceON is true")
            p.prints("[AspectConfiguration] DiversificationON is true")
            p.prints("[AspectConfiguration] BackupON is true")
            self.args_to_save = self.args_to_save + '-ft d b'
            DecoratorConfiguration.FTBackup_on = True
            DecoratorConfiguration.FTDiversification_on = True

        if '-ft b r' in args or '-ft r b' in args and not None:
            p.prints("[AspectConfiguration] FaultToleranceON is true")
            if tag == "server":
                p.prints("[AspectConfiguration] BackupON is true")
                DecoratorConfiguration.FTBackup_on = True
            elif tag == "client":
                p.prints("[AspectConfiguration] RetryON is true")
                DecoratorConfiguration.FTRetry_on = True
            self.args_to_save = self.args_to_save + '-ft b r'

        if args.__contains__('-res') is True:
            print("""* Please, write down the name of your file:""")
            try:
                while True:
                    name = input()
                    if name == "":
                        print("""* You should write the correct name""")
                        continue

                    get_options(name)

                    if args is None:
                        print("""* You should write the correct name""")
                        continue

                    else:
                        print("""* Returned options are: {}""".format(args))
                        self.check_string(args, tag)
                        break

            except KeyboardInterrupt:
                exit(0)

        if DecoratorConfiguration.FTBackup_on is True:
            print("""
* Please, choose a name for your file, for example, 'SaveCmdOptions':""")
            name = input()
            save_options(name, self.args_to_save)


if __name__ == '__main__':
    intro = pyfiglet.figlet_format("SORT SYSTEM\n"
                                   "DISCA\n"
                                   "UPV 2020", font='bulbhead')
    intro_uni = """
*********************************************************************************************
*
* Master Universitario en Ingenieria de Computadores y Redes
* http://www.upv.es/titulaciones/MUIC
* Universitat Politecnica de Valencia (UPV)
*
*********************************************************************************************
* Select the service you want to execute:
*
*     1.- server.Server
*     2.- client.Client
*     3.- server.ServerStop
*     4.- END
*
"""
    OptionsParser().cmdloop(intro=intro + intro_uni)
