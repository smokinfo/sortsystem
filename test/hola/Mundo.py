

# DECORADOR
def mundo(func):
    def function_wrapper():

        # START BEFORE FUNCTION
        print("¡¡¡", end='')
        # END BEFORE FUNCTION

        # FUNCTION (PROCEED)
        func()

        # START AFTER FUNCTION
        hola_mundo()
        print("!!! \n")
        # END AFTER FUNCTION

    return function_wrapper


# Podemos usar otros metodos dentro de decorators
def hola_mundo():
    print(" Mundo", end='')
