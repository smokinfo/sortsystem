import sys
from test.hola.Mundo import mundo
from test.hola.Decir import decir
from test.hola.Test import test


# FUNCCION PARA DECORAR
@test
@mundo
def saluda():
    sys.stdout.write("Hola")


st = "Me piden que diga algo"
str_arr = ["Me gustaría decir algo"]


saluda()
decir(str_arr)
print("array: ", str_arr)

# Observaciones: no se puede usar los decoradores para las funciones internas.
# Hay que implementar un decorador para cada funcion que está afectada.
