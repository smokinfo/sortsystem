

# DECORADOR
def obey(_):
    def wrap(func):
        def wrapped_func(*args):

            print("Controlamos no solo lo que dices, sino tambien cuando y como lo dices: ")

            if isinstance(args[0], str):
                pass
                # Obtenemos el array de variables que pasamos a decir(s)...
                # ...y la primera posición la convertimos en string
                string = str(args[0])  # args[0].soString()

            else:
                # O obtenemos el array que pasamos
                array_of_args = args[0]
                string = str(array_of_args[0])

            # para hacer replace de palabras
            res = [string.replace("algo", "esto, la UPV es la mejor!!!")]

            # TAMBIEN ESTE RETURN SE ACTUA COMO PROCEED
            return func(res)
        return wrapped_func
    return wrap
