# Array para tests
def test_array():
    test_arr = [572, 1, -42, 53, 11, 729, 3, -11, 194, 0, -22332]
    return test_arr


# DECORADOR
def test(func):
    def w_f():
        print("\nTESTING DEL SEGUNDO DECORADOR")
        print("----------------------------- \n")
        func()
    return w_f
