# SortSystem/server/server.py

from concurrent.futures import ThreadPoolExecutor
import socket
from SortSystem.server.serverthread import ServerThread
from decorators.test_decorators.backup_decorator_new import backup_decorator
from utils.sortingalgorithm.sortingalgorithmfactory import SortingAlgorithmFactory, alg_type
from SortSystem.utils.utils import prints as p
from utils.utils.helper import Helper


port = 14505
client_number = 1
host = '127.0.0.1'
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind((host, port))
server.listen(10)


@backup_decorator("set")
def set_client_number(client_n):
    # @BackupDecorator("set", cn)
    # def init_set_client_number(cl_nu=cn):
    client_number = client_n


@backup_decorator("get")
def get_client_number():
    # @BackupDecorator("get")
    # def init_get_client_number():
    return client_number

    # return init_get_client_number


def server_start():
    client_number = get_client_number()
    executor = ThreadPoolExecutor()
    p.prints("> [Server.start] Hi!!! Starting the sorting server at port: {}".format(port))
    while Helper.stop is not True:

        p.prints("> [Server.start] Waiting for a new client ...")
        client, addr = server.accept()
        p.prints("> [Server.start] Processing request for client #{}...".format(client_number))
        set_client_number(int(client_number) + 1)
        client_number = get_client_number()

        sst = ServerThread(client, SortingAlgorithmFactory.new_sa(alg_type.BUBBLESORT))
        p.prints(">> Serving the client in a thread ...")
        future = executor.submit(sst.run, client)
        future.result()
        if Helper.stop is True:
            break

    p.prints("> [Server.start] Server is out of the do while loop ... bye!!!")
    executor.shutdown()
    p.prints("> [Server.start] All threads in the pool terminated ... bye!!!")
    stop_server()


def stop_server():
    p.prints("> [Server.stopServer] Stopping the server ...")
    if server is not None:
        p.prints("> [Server.stopServer] Closing the socket ...")
        server.close()
    p.prints("> [Server.stopServer] Setting stop variable to true ...")
