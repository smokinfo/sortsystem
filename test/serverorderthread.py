from utils.comlib.socketcommunication import SocketCommunication
from SortSystem.utils.auxiliar import prints as p


class ServerOrderThread:
    def __init__(self, s, server):
        self.server = server
        self.socket = s
        self.stop = False
        self.port = 4321
        self.host = s.gethostname()

    def run(self):
        self.socket.bind((self.host, self.port))
        self.socket.listen(10)
        while self.stop is not True:
            p.prints(">> [StopThread] Waiting for an STOP order ...")
            client, address = self.socket.accept()
            channel = SocketCommunication(client)
            p.prints(">> [StopThread] Something is received ...")
            message = channel.read_message_from_socket()
            p.prints(">> [StopThread] Received message is {}".format(str(message)))
            if self.stop is True:
                break

        p.prints(">> [StopThread] Done ending the thread execution ...")
        self.socket.close()

    def process_stop(self):
        p.prints(">> [StopThread] Processing the STOP order ...")
        self.stop = True
        self.server.stop_server()
