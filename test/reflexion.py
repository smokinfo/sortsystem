x = "Hola Mundo!"
print("x =", type(x))  # Output: x = <class 'str'>

x = 14
print("x =", type(x))  # Output: x = <class 'int'>

x = ["Hola Mundo!"]
print("x =", type(x))  # Output: x = <class 'list'>

x = b'14'
print("x =", type(x))  # Output: x = <class 'bytes'>

