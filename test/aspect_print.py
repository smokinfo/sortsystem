# SortSystem/decorators/aspect_print.py.py
import utils.randomgen.randomizer


class ChangeString(object):
    def __init__(self, f):
        self.f = f

    def __call__(self, f):
        def wrapped_f(*args):
            print("args before: ", args)
            arg = list(args)
            val2 = " País!"
            val3 = " Nación!"
            val4 = " Igor!"
            switch = utils.randomgen.randomizer.random_integer_bound(3)

            if switch == 0:
                f(args)
            elif switch == 1:
                arg[0] = val2
            elif switch == 2:
                arg[0] = val3
            elif switch == 3:
                arg[0] = val4

            f(arg)
            print("args after: ", arg)

        return wrapped_f


class Otro(object):
    def __init__(self, f):
        self.f = f

    def __call__(self):
        print("Lo hemos llamado 1 vez")
        self.f()


class Otro2(object):
    def __init__(self, f):
        self.f = f

    def __call__(self):
        print("Lo hemos llamado 2 vez")
        self.f()
